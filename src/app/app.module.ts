import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormComponent } from './Components/form/form.component';
import { AboutstartupComponent } from './Components/aboutstartup/aboutstartup.component';
import { ContactComponent } from './Components/contact/contact.component';
import { HeaderComponent } from './Components/header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TopinputareaComponent } from './Components/topinputarea/topinputarea.component';

import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BottominputareaComponent } from './Components/bottominputarea/bottominputarea.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    AboutstartupComponent,
    ContactComponent,
    HeaderComponent,
    SidebarComponent,
    TopinputareaComponent,
    BottominputareaComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
