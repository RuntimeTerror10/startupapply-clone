import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  isAboutStartupFormOpen: boolean = true;

  @Output() onAboutStartUpFormOpen = new EventEmitter<boolean>();
  @Output() onContactFormOpen = new EventEmitter<boolean>();

  goToAboutStartUp() {
    this.isAboutStartupFormOpen = true;
    this.onAboutStartUpFormOpen.emit();
  }
  goToContactForm() {
    this.isAboutStartupFormOpen = false;
    this.onContactFormOpen.emit();
  }

  constructor() {}

  ngOnInit(): void {}
}
