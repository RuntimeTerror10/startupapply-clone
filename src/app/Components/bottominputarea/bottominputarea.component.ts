import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

export interface Interest {
  name: string;
  completed: boolean;
  color: string;
  subinterests?: Interest[];
}
@Component({
  selector: 'app-bottominputarea',
  templateUrl: './bottominputarea.component.html',
  styleUrls: ['./bottominputarea.component.css'],
})
export class BottominputareaComponent implements OnInit {
  charCount = 1200;
  aboutStartup: string = '';
  isBootStrapped: boolean = true;
  stage: string = '';

  @Output() initAboutStartup = new EventEmitter();
  @Output() initStageToogle = new EventEmitter();
  @Output() initStageButtonToggle = new EventEmitter();
  @Output() initInterests = new EventEmitter();

  toggleBootstrap() {
    if (this.isBootStrapped) {
      return;
    } else {
      this.isBootStrapped = true;
    }
    this.initStageToogle.emit(
      `${this.isBootStrapped ? 'BootStrapped' : 'Funded'}`
    );
  }

  toggleFunded() {
    if (this.isBootStrapped === false) {
      return;
    } else {
      this.isBootStrapped = false;
    }
    this.initStageToogle.emit(
      `${this.isBootStrapped ? 'BootStrapped' : 'Funded'}`
    );
  }

  updateCharCount() {
    this.charCount = 1200 - this.aboutStartup.length;
    this.initAboutStartup.emit(this.aboutStartup);
  }

  getActiveBtn(data: any) {
    console.log(data);
    this.initStageButtonToggle.emit(data.value);
  }

  interest: Interest = {
    name: 'All',
    completed: false,
    color: 'primary',
    subinterests: [
      {
        name: 'Incubation',
        completed: false,
        color: 'primary',
      },
      {
        name: 'Accelaration',
        completed: false,
        color: 'primary',
      },
      {
        name: 'Investment',
        completed: false,
        color: 'primary',
      },
      {
        name: 'mentoring',
        completed: false,
        color: 'primary',
      },
    ],
  };

  allComplete: boolean = false;

  updateAllComplete() {
    this.allComplete =
      this.interest.subinterests != null &&
      this.interest.subinterests.every((t) => t.completed);
    let selectedInterests = this.interest.subinterests.filter(
      (t) => t.completed
    );
    this.initInterests.emit(selectedInterests);
  }

  setAll(completed: boolean) {
    this.allComplete = completed;
    if (this.interest.subinterests == null) {
      return;
    }
    this.interest.subinterests.forEach((t) => (t.completed = completed));
    let allInterests = this.interest.subinterests.map((t) => t.name);
    this.initInterests.emit(allInterests);
  }
  constructor() {}

  ngOnInit(): void {}
}
