import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BottominputareaComponent } from './bottominputarea.component';

describe('BottominputareaComponent', () => {
  let component: BottominputareaComponent;
  let fixture: ComponentFixture<BottominputareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BottominputareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BottominputareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
