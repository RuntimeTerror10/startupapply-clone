import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  isAboutStartupFormOpen: boolean = true;
  first_name: string = '';
  last_name: string = '';
  email: string = '';
  mobile: string = '';
  state: string = '';
  city: string = '';
  website: string = '';
  mobileAppLink: string = '';

  payload = {
    startupName: '',
    industry: '',
    sector: '',
    stageToggle: '',
    stageButtonToggle: '',
    aboutStartup: '',
    interest: [],
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    state: '',
    city: '',
    website: '',
    mobileAppLink: '',
  };

  saveStartupInfo(data: any) {
    this.payload.startupName = data.startupName;
    this.payload.industry = data.industry;
    this.payload.sector = data.sector;
    console.log(this.payload);
  }

  goToContactForm() {
    this.isAboutStartupFormOpen = false;
  }
  openContactForm() {
    this.isAboutStartupFormOpen = false;
  }
  openStartUpForm() {
    this.isAboutStartupFormOpen = true;
  }
  saveAboutStartup(data: any) {
    this.payload.aboutStartup = data;
    // console.log(this.payload);
  }
  saveStageToggle(data: any) {
    this.payload.stageToggle = data;
  }
  saveStageButtonToggle(data: any) {
    this.payload.stageButtonToggle = data;
  }
  saveInterests(data: any) {
    this.payload.interest = data;
    console.log(this.payload);
  }
  saveFirstName() {
    this.payload.firstName = this.first_name;
  }
  saveLastName() {
    this.payload.lastName = this.last_name;
  }
  saveEmail() {
    this.payload.email = this.email;
  }
  saveMobile() {
    this.payload.mobile = this.mobile;
  }
  saveState() {
    this.payload.state = this.state;
  }
  saveCity() {
    this.payload.city = this.city;
  }
  saveWebsite() {
    this.payload.website = this.website;
  }
  saveMobileAppLink() {
    this.payload.mobileAppLink = this.mobileAppLink;
    console.log(this.payload);
  }

  handleSubmit() {
    let test = Object.keys(this.payload)
      .map(
        (key) =>
          `${encodeURIComponent(key)}=${encodeURIComponent(this.payload[key])}`
      )
      .join('&');
    fetch(`https://api965874.herokuapp.com/?${test}`, {
      method: 'POST',
    })
      .then((res) => {
        if (res.status === 400) {
          alert('Please fill all the fields');
        } else {
          alert('Thank you for your response');
        }
      })
      .catch((err) => {
        alert(err);
      });

    console.log('Hi');
  }
  constructor() {}

  ngOnInit(): void {}
}
