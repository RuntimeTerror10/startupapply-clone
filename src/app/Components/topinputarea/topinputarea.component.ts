import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-topinputarea',
  templateUrl: './topinputarea.component.html',
  styleUrls: ['./topinputarea.component.css'],
})
export class TopinputareaComponent implements OnInit {
  name: string = '';
  sector: string = '';
  industry: string = '';

  temp = {
    startupName: '',
    industry: '',
    sector: '',
  };

  @Output() startupInfo = new EventEmitter();

  initStartupName() {
    this.temp.startupName = this.name.trim();
    this.startupInfo.emit(this.temp);
  }
  initIndustry() {
    this.temp.industry = this.industry.trim();
    this.startupInfo.emit(this.temp);
  }
  initSector() {
    this.temp.sector = this.sector.trim();
    this.startupInfo.emit(this.temp);
  }
  constructor() {}

  ngOnInit(): void {}
}
