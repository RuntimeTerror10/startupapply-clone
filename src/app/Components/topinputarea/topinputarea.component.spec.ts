import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopinputareaComponent } from './topinputarea.component';

describe('TopinputareaComponent', () => {
  let component: TopinputareaComponent;
  let fixture: ComponentFixture<TopinputareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopinputareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopinputareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
