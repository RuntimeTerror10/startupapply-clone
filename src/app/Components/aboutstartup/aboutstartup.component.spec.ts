import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutstartupComponent } from './aboutstartup.component';

describe('AboutstartupComponent', () => {
  let component: AboutstartupComponent;
  let fixture: ComponentFixture<AboutstartupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutstartupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutstartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
