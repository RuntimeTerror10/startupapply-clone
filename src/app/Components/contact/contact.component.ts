import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit {
  first_name: string = '';
  last_name: string = '';
  email: string = '';
  phone_number: string = '';
  state_name: string = '';
  city_name: string = '';
  website_url: string = '';
  mobile_app_link: string = '';

  contactPayload = {
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    state: '',
    city: '',
    website: '',
    mobileAppLink: '',
  };

  initFirstName() {
    console.log(this.first_name);
  }
  constructor() {}

  ngOnInit(): void {}
}
